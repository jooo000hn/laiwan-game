package com.java80.littlegame.service.game.desk;

import java.util.TimerTask;

import com.java80.littlegame.common.msg.timer.TimerMessage;

public class DeskTask extends TimerTask {
	private TimerMessage msg;
	public Desktop desk;
	@Override
	public void run() {
		desk.onTimer(msg);
	}
	public DeskTask(TimerMessage msg, Desktop desk) {
		super();
		this.msg = msg;
		this.desk = desk;
	}
}
