package com.java80.littlegame.common.msg.timer;

import com.java80.littlegame.common.msg.BaseMsg;
import com.java80.littlegame.common.msg.ProtoList;

public abstract class TimerMessage extends BaseMsg {
	private String targetQueue;
	private long delay;

	public long getDelay() {
		return delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}

	public String getTargetQueue() {
		return targetQueue;
	}

	public void setTargetQueue(String targetQueue) {
		this.targetQueue = targetQueue;
	}

	@Override
	public int getType() {
		return ProtoList.MSG_TYPE_TIMER;
	}

}
