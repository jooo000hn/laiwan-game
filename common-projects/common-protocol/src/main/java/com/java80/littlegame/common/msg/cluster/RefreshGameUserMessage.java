package com.java80.littlegame.common.msg.cluster;

import com.java80.littlegame.common.msg.ProtoList;

public class RefreshGameUserMessage extends ClusterMessage {

	@Override
	public int getCode() {
		return ProtoList.MSG_CODE_REFRESH_GAMEUSER;
	}

	private String sourceServiceId;// 哪个服务节点改的
	private long userId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getSourceServiceId() {
		return sourceServiceId;
	}

	public void setSourceServiceId(String sourceServiceId) {
		this.sourceServiceId = sourceServiceId;
	}

}
