package com.java80.littlegame.common.msg.hall;

import com.java80.littlegame.common.msg.BaseMsg;
import com.java80.littlegame.common.msg.ProtoList;

public class UserRegisterMessage extends BaseMsg {
	private String nickName;
	private String loginName;
	private String password;
	private int result;

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int getType() {
		return ProtoList.MSG_TYPE_HALL;
	}

	@Override
	public int getCode() {
		return ProtoList.MSG_CODE_REGISTER;
	}

}
