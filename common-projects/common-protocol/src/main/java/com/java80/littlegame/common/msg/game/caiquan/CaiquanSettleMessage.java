package com.java80.littlegame.common.msg.game.caiquan;

import com.java80.littlegame.common.msg.BaseMsg;
import com.java80.littlegame.common.msg.ProtoList;

public class CaiquanSettleMessage extends BaseMsg {

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return ProtoList.MSG_TYPE_GAME;
	}

	@Override
	public int getCode() {
		// TODO Auto-generated method stub
		return ProtoList.MSG_CODE_CAIQUANASETTLE;
	}

	private long winner;// 没有就平局

	public long getWinner() {
		return winner;
	}

	public void setWinner(long winner) {
		this.winner = winner;
	}

}
