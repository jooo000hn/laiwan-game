package com.java80.littlegame.common.base.data;

import java.util.List;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.java80.littlegame.common.orm.model.SGameConfig;
import com.java80.littlegame.common.orm.model.SGameInfo;
import com.java80.littlegame.common.service.system.SystemService;

public class SystemDataMgr {
	final transient static Logger log = LoggerFactory.getLogger(SystemDataMgr.class);
	private static TreeMap<Integer, SGameInfo> gameInfos = new TreeMap<>();
	private static TreeMap<Integer, SGameConfig> gameConfigs = new TreeMap<>();

	public static void loadSystemData() {
		List<SGameInfo> sgameInfos = null;
		try {
			sgameInfos = SystemService.getInstance().findAllGameInfo();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (SGameInfo i : sgameInfos) {
			gameInfos.put(i.getId(), i);
		}

		List<SGameConfig> sgameconfigs = null;
		try {
			sgameconfigs =  SystemService.getInstance().findAllGameConfig();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (SGameConfig i : sgameconfigs) {
			gameConfigs.put(i.getGameId(), i);
		}
	}

	public static TreeMap<Integer, SGameInfo> getGameInfos() {
		return gameInfos;
	}

	public static TreeMap<Integer, SGameConfig> getGameConfigs() {
		return gameConfigs;
	}

}
