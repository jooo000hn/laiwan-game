package com.java80.littlegame.common.base.vertx;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class ServiceStatusHelper {
	private static Random rand = new Random();
	private static ConcurrentHashMap<String, ServiceStatus> servicestatus = new ConcurrentHashMap<>();

	public static ConcurrentHashMap<String, ServiceStatus> getAllServiceStatus() {
		return servicestatus;
	}

	public static void addServiceStatus(ServiceStatus ss) {
		if (ss != null)
			servicestatus.put(ss.getServiceId(), ss);
		System.out.println("服务状态最新(add)：" + servicestatus.values());
	}

	public static void removeServiceStatus(String serviceId) {
		if (serviceId != null)
			servicestatus.remove(serviceId);
		System.out.println("服务状态最新(remove)：" + servicestatus.values());
	}

	/** 随机一个服务 */
	public static ServiceStatus randServiceStatus(int serviceType) {
		// 现在有几个当前的服务状态
		List<ServiceStatus> sss = new ArrayList<>();
		for (ServiceStatus ss : servicestatus.values()) {
			if (ss.getServiceType() == serviceType) {
				sss.add(ss);
			}
		}

		return sss.size() == 1 ? sss.get(0) : sss.get(randNum(0, sss.size() - 1));
	}

	public static List<ServiceStatus> findServiceStatus(int serviceType) {
		// 现在有几个当前的服务状态
		List<ServiceStatus> sss = new ArrayList<>();
		for (ServiceStatus ss : servicestatus.values()) {
			if (ss.getServiceType() == serviceType) {
				sss.add(ss);
			}
		}
		return sss;
	}

	public static ServiceStatus findServiceStatus(String serviceId) {
		for (ServiceStatus ss : servicestatus.values()) {
			if (ss.getServiceId().equals(serviceId)) {
				return ss;
			}
		}
		return null;
	}

	private static int randNum(int min, int max) {
		return rand.nextInt(max - min + 1) + min;
	}
}
