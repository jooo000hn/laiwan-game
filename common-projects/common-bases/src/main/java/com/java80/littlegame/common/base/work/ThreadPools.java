package com.java80.littlegame.common.base.work;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPools {
	private static ExecutorService fixedThreadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()*3);

	public static void addTask(final Task task) {
		fixedThreadPool.execute(new Runnable() {
			public void run() {
				task.work();
			}
		});
	}
}
