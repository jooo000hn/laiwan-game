package com.java80.littlegame.common.base.data;

import java.io.Serializable;

import com.java80.littlegame.common.cache.CacheObj;
import com.java80.littlegame.common.consts.CachePrefixConsts;

public class GameUser extends CacheObj implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7312914784693240088L;
	private long userId;
	private String gatewayServiceId;
	private String hallServiceId;
	private String gameServiceId;
	private String sessionId;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getGameServiceId() {
		return gameServiceId;
	}

	public void setGameServiceId(String gameServiceId) {
		this.gameServiceId = gameServiceId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getGatewayServiceId() {
		return gatewayServiceId;
	}

	public void setGatewayServiceId(String gatewayServiceId) {
		this.gatewayServiceId = gatewayServiceId;
	}

	public String getHallServiceId() {
		return hallServiceId;
	}

	public void setHallServiceId(String hallServiceId) {
		this.hallServiceId = hallServiceId;
	}

	@Override
	public String toString() {
		return "GameUser [userId=" + userId + ", gatewayServiceId=" + gatewayServiceId + ", hallServiceId="
				+ hallServiceId + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GameUser) {
			return this.getUserId() == ((GameUser) obj).getUserId();
		}
		return false;

	}

	@Override
	public String getKeyName() {
		return CachePrefixConsts.CACHE_KEY_GAMEUSER + userId;
	}

}
