package com.java80.littlegame.common.consts;

public class CachePrefixConsts {
	/**
	 * 对象缓存key profix
	 */
	public static final String CACHE_PROFIX_OBJECT = "Cache:Obj:";
	/**
	 * 对象list缓存key profix
	 */
	public static final String CACHE_PROFIX_LIST = "Cache:list:";
	/**
	 * 对象set缓存key profix
	 */
	public static final String CACHE_PROFIX_SET = "Cache:set:";
	/**
	 * 对象map缓存key profix
	 */
	public static final String CACHE_PROFIX_MAP = "Cache:map:";

	public static final String CACHE_KEY_GAMEUSER = "gameuser";
}
