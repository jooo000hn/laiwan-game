package com.java80.littlegame.common.orm.mapper.base.annotation;

public enum TableIDTypeEnum {
	/** uuid 带下划线的 */
	UUID_,
	/** UUID不带下划线 */
	UUID,
	/** 全局ID 雪花算法(暂未实现) */
	GLOBAL,
	/** 数据库ID */
	DBID,
	/**其他*/
	OTHER;
}
