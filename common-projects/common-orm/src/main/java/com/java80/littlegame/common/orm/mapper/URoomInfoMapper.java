package com.java80.littlegame.common.orm.mapper;

import com.java80.littlegame.common.orm.mapper.base.BaseMapper;
import com.java80.littlegame.common.orm.model.URoomInfo;

public interface URoomInfoMapper extends BaseMapper<URoomInfo> {

}
