package com.java80.littlegame.common.orm.mapper;

import com.java80.littlegame.common.orm.mapper.base.BaseMapper;
import com.java80.littlegame.common.orm.model.SGameConfig;

public interface SGameConfigMapper extends BaseMapper<SGameConfig> {

}
