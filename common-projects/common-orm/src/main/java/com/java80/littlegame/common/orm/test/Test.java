package com.java80.littlegame.common.orm.test;

import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.java80.littlegame.common.orm.mapper.UUserInfoMapper;
import com.java80.littlegame.common.orm.model.UUserInfo;

public class Test {
	 

	public static void main(String[] args) throws Exception {
		SqlSessionFactory factory=getFactory(new Test());
        SqlSession session=factory.openSession();
        //使用反射的方法
        UUserInfoMapper mapper=session.getMapper(UUserInfoMapper.class);
        List<UUserInfo> selectList = mapper.findAll(new UUserInfo());
        System.out.println(selectList);
        System.out.println("selectList:"+selectList.size());
        
        //
        try {
			UUserInfo u1=new UUserInfo();
			mapper.insert(u1);
			//int a=1/0;
			UUserInfo u2=new UUserInfo();
			mapper.insert(u2);
			//session.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        session.close();
	}
	 public static SqlSessionFactory getFactory( Test test) throws Exception{
	        String resource="mybatis.xml";
	        //加载mybatis 的配置文件（它也加载关联的映射文件）
	        InputStream is=Test.class.getClassLoader().getResourceAsStream(resource);
	        //构建sqlSession 的工厂
	        SqlSessionFactory factory=new  SqlSessionFactoryBuilder().build(is);
	        
	        //MybatisSqlSessionFactoryBean sqlSessionFactoryBean = test.sqlSessionFactoryBean();
	        return factory;
	    }
	
	  
}
