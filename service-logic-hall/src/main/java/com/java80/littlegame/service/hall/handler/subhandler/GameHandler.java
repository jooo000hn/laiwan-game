package com.java80.littlegame.service.hall.handler.subhandler;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import com.java80.littlegame.common.base.data.GameUser;
import com.java80.littlegame.common.base.data.GameUserMgr;
import com.java80.littlegame.common.base.data.SystemDataMgr;
import com.java80.littlegame.common.base.vertx.ServiceStatusHelper;
import com.java80.littlegame.common.base.vertx.VertxMessageHelper;
import com.java80.littlegame.common.msg.hall.PullAndPushGameSystemMessage;
import com.java80.littlegame.common.msg.hall.PullAndPushGameSystemMessage.GameInfo;
import com.java80.littlegame.common.orm.model.SGameInfo;

public class GameHandler {
	public void pullAndpushgamesystem(PullAndPushGameSystemMessage msg) {
		TreeMap<Integer, SGameInfo> gameInfoMap = SystemDataMgr.getGameInfos();
		List<GameInfo> gameInfos = new ArrayList<>();
		for (SGameInfo sgameinfo : gameInfoMap.values()) {
			GameInfo gi = new GameInfo();
			gi.setGameName(sgameinfo.getGameName());
			gi.setId(sgameinfo.getId());
			gameInfos.add(gi);
		}
		msg.setGameInfos(gameInfos);
		long userId = msg.getSenderUserId();

		GameUser gameUser = GameUserMgr.getGameUser(userId);
		VertxMessageHelper.sendMessageToService(
				ServiceStatusHelper.findServiceStatus(gameUser.getGatewayServiceId()).getServiceQueueName(),
				msg.toString());

	}
}
