package com.java80.littlegame.service.gateway.websocket;

import com.java80.littlegame.common.base.data.GameUser;
import com.java80.littlegame.common.base.data.GameUserMgr;
import com.java80.littlegame.common.base.vertx.VertxMessageHelper;
import com.java80.littlegame.common.base.work.BaseHandler;
import com.java80.littlegame.common.base.work.Task;
import com.java80.littlegame.common.base.work.ThreadPools;
import com.java80.littlegame.common.msg.BaseMsg;
import com.java80.littlegame.common.msg.ProtoHelper;
import com.java80.littlegame.common.msg.ProtoList;
import com.java80.littlegame.common.msg.cluster.RefreshGameUserMessage;
import com.java80.littlegame.common.msg.hall.UserLoginMessage;
import com.java80.littlegame.service.gateway.session.ChannelManageCenter;

public class GatewayHandler extends BaseHandler {
	private GatewayHandler() {
	}

	private static GatewayHandler ins = new GatewayHandler();

	public static GatewayHandler getInstance() {
		return ins;
	}

	public void onMessage(BaseMsg msg) {
		switch (msg.getType()) {
		case ProtoList.MSG_TYPE_HALL:
			processHallMsg(msg);
			break;
		case ProtoList.MSG_TYPE_CLUSTER:
			processClusterMsg(msg);
			break;
		default:
			MessageHelper.outMsg(msg);
			break;
		}

	}

	private void processClusterMsg(BaseMsg msg) {
		switch (msg.getCode()) {
		case ProtoList.MSG_CODE_REFRESH_GAMEUSER:
			refreshGameUser((RefreshGameUserMessage) msg);
			break;

		default:
			break;
		}

	}

	private void processHallMsg(BaseMsg msg) {
		switch (msg.getCode()) {
		case ProtoList.MSG_CODE_LOGIN:
			doLogin((UserLoginMessage) msg);
			break;
		default:
			MessageHelper.outMsg(msg);
			break;
		}

	}

	public void refreshGameUser(RefreshGameUserMessage msg) {
		if (!GatewayConfig.getServiceId().equals(msg.getSourceServiceId())) {
			GameUserMgr.refreshGameUser(msg.getUserId());
		}
	}

	private void doLogin(UserLoginMessage msg) {
		int result = msg.getResult();
		// 登录之后的消息是多播的，需要鉴别
		long userId = msg.getRecUserId();
		GameUser gameUser = GameUserMgr.getGameUser(userId);

		if (result == 1) {
			boolean bind = ChannelManageCenter.getInstance().bind(msg.getSessionId(), userId);
			if (bind) {
				gameUser.setGatewayServiceId(GatewayConfig.getServiceId());
				gameUser.setSessionId(msg.getSessionId());
				GameUserMgr.addGameUser(gameUser, () -> {
					// 发送刷新通知
					RefreshGameUserMessage refreshGameUserMessage = new RefreshGameUserMessage();
					refreshGameUserMessage.setUserId(gameUser.getUserId());
					refreshGameUserMessage.setSourceServiceId(GatewayConfig.getServiceId());
					VertxMessageHelper.broadcastMessageToAllService(refreshGameUserMessage);
				});
			}
		}
		MessageHelper.outMsg(msg);
	}

	@Override
	public Object onReceive(String obj) {
		ThreadPools.addTask(new Task() {

			@Override
			public void work() {
				onMessage(ProtoHelper.parseJSON(obj.toString()));
			}
		});
		return null;
	}

}
